package com.uhu.mvc.server;

import com.uhu.mvc.router.PathRouter;
import com.uhu.mvc.servlet.DispatchServlet;
import io.undertow.Undertow;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ServletInfo;
import io.undertow.servlet.core.ServletContainerImpl;
import io.undertow.servlet.util.ImmediateInstanceFactory;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;

/**
 * @Description: 基于undertow的server
 * @Name: UndertowWebServer
 * @Author: Bomber
 * @CreateTime: 2023/10/16 16:53
 */
@Slf4j
public class UndertowWebServer extends AbstractWebServer {

    public UndertowWebServer(PathRouter router) {
        super(router);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void run() {
        try {
            checkRouter();
            DispatchServlet dispatchServlet = new DispatchServlet(router);

            ServletInfo servletInfo = new ServletInfo("dispatchServlet", DispatchServlet.class, new ImmediateInstanceFactory<>(dispatchServlet));
            servletInfo.addMapping("/");

            DeploymentInfo deploymentInfo = new DeploymentInfo();
            deploymentInfo.addServlet(servletInfo);
            deploymentInfo.setDeploymentName("MY-MVC");
            deploymentInfo.setContextPath("/");
            deploymentInfo.setClassLoader(DispatchServlet.class.getClassLoader());

            ServletContainerImpl servletContainer = new ServletContainerImpl();
            DeploymentManager deploymentManager = servletContainer.addDeployment(deploymentInfo);
            deploymentManager.deploy();

            Undertow undertow = Undertow.builder()
                    .addHttpListener(port, host)
                    .setHandler(deploymentManager.start())
                    .build();

            log.info("undertow web start in [{}:{}]", host, port);
            undertow.start();

        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
}
