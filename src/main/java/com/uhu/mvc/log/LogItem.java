package com.uhu.mvc.log;

/**
 * @Description: 日志元素
 * @Name: LogItem
 * @Author: Bomber
 * @CreateTime: 2023/10/16 13:02
 */
public enum LogItem {
    TIME("%d{HH:mm:ss.SSS}"),
    THREAD("[%t]"),
    LEVEL("%-5level"),
    CLASSNAME("%C"),
    LOGGER("%logger"),
    MSG("%msg"),
    FILE("%F"),
    LINE_NO("%L");

    private final String value;

    LogItem(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
