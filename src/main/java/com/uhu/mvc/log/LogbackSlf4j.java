package com.uhu.mvc.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LogbackServiceProvider;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.spi.AppenderAttachableImpl;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @Description: 日志提供者
 * @Name: Logback
 * @Author: Bomber
 * @CreateTime: 2023/10/16 9:57
 */
public class LogbackSlf4j {

    private static final Map<Object, String> colorNameMap = new HashMap<>();

    static {
        Object o = new Object();
        Arrays.stream(Color.class.getDeclaredFields())
                .filter(field -> (field.getType().equals(Color.class)))
                .forEach(field -> {
                    try {
                        Object color = field.get(o);
                        String colorName = field.getName().toLowerCase(Locale.ROOT);
                        colorNameMap.put(color, colorName);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }

    private final Level level;
    private final String pattern;

    static {
        LoggerFactory.getLogger(LogbackSlf4j.class.getTypeName());
    }

    private LogbackSlf4j(Level level, String pattern) {
        this.level = level;
        this.pattern = pattern;
    }

    /**
     * 使用此日志
     */
    public void use() {
        try {
            Field field = LoggerFactory.class.getDeclaredField("PROVIDER");
            field.setAccessible(true);
            LogbackServiceProvider provider = (LogbackServiceProvider) field.get(new Object());

            Field defaultLoggerContext = LogbackServiceProvider.class.getDeclaredField("defaultLoggerContext");
            defaultLoggerContext.setAccessible(true);
            LoggerContext loggerContext = (LoggerContext) defaultLoggerContext.get(provider);

            LoggerContext newLoggerContext = getNewLoggerContext(loggerContext);
            defaultLoggerContext.setAccessible(true);
            defaultLoggerContext.set(provider, newLoggerContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private LoggerContext getNewLoggerContext(LoggerContext loggerContext) throws Exception {
        Field field = LoggerContext.class.getDeclaredField("root");
        field.setAccessible(true);
        Logger root = (Logger) field.get(loggerContext);
        root.setLevel(level);
        field.set(loggerContext, root);

        Field aai = Logger.class.getDeclaredField("aai");
        aai.setAccessible(true);
        AppenderAttachableImpl<?> appenderAttachable = (AppenderAttachableImpl<?>) aai.get(root);
        ConsoleAppender<ILoggingEvent> appender = (ConsoleAppender<ILoggingEvent>) appenderAttachable.getAppender("MY-MVC");
        PatternLayoutEncoder encoder = (PatternLayoutEncoder) appender.getEncoder();
        encoder.setPattern(pattern);
        encoder.stop();
        encoder.start();
        return loggerContext;
    }

    /**
     * 建造者
     * @return 建造者对象
     */
    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private Level level = Level.ALL;
        private StringBuilder pattern = new StringBuilder();


        public Builder() {
        }

        public Builder setLevel(Level level) {
            this.level = level;
            return this;
        }

        /**
         * 添加日志元素
         * @param item
         * @return
         */
        public Builder appendLogItem(LogItem item) {
            if (item.equals(LogItem.LEVEL)) {
                return appendLogItem(item, "highlight");
            }
            return appendLogItem(item, Color.WHITE);
        }

        /**
         * 添加日志元素
         * @param item
         * @param color
         * @return
         */
        public Builder appendLogItem(LogItem item, Color color) {
            return appendLogItem(item, colorNameMap.get(color));
        }

        /**
         * 添加日志元素
         * @param item
         * @param color
         * @return
         */
        public Builder appendLogItem(LogItem item, String color) {
            if ("white".equals(color)) pattern
                        .append(item.getValue())
                        .append(" ");
            else pattern
                        .append("%")
                        .append(color)
                        .append("(")
                        .append(item.getValue())
                        .append(")")
                        .append(" ");
            return this;
        }

        /**
         * 构建方法
         * @return
         */
        public LogbackSlf4j build() {
            if (pattern.isEmpty()) {
                pattern.append("%d{HH:mm:ss.SSS} %highlight(%-5level) %boldYellow(%thread) %boldGreen(%logger) %msg%n");
            }
            return new LogbackSlf4j(level, pattern.toString() + "\n");
        }
    }
}
